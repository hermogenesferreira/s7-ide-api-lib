"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@tsed/common");
const fs_1 = require("fs");
const Path = require("path");
const LanguageMiddleware_1 = require("./middlewares/LanguageMiddleware");
let config = {
    acceptMimes: ["application/json"],
    rootDir: Path.resolve(Path.join(process.cwd(), "/src/app")),
    httpPort: process.env.PORT,
    componentsScan: [
        "${rootDir}/middlewares/**/*.ts",
        "${rootDir}/services/**/*.ts",
    ],
    swagger: {
        path: "/api-docs",
    },
    mount: {
        "/": "${rootDir}/controllers/**/*.ts",
    },
};
try {
    config = Object.assign(config, JSON.parse(fs_1.readFileSync(Path.join(process.cwd(), "tsed.json"), "utf8")));
    // tslint:disable-next-line:no-empty
}
catch (_a) { }
let Server = class Server extends common_1.ServerLoader {
    constructor(beforeRoutesInitFn) {
        super();
        this.beforeRoutesInitFn = beforeRoutesInitFn;
    }
    $onMountingMiddlewares() {
        const cookieParser = require("cookie-parser");
        const bodyParser = require("body-parser");
        const compress = require("compression");
        const methodOverride = require("method-override");
        const serveStatic = require("serve-static");
        this
            .use(common_1.GlobalAcceptMimesMiddleware)
            .use(cookieParser())
            .use(compress({}))
            .use(methodOverride())
            .use(bodyParser.json())
            .use(LanguageMiddleware_1.LanguageMiddleware)
            .use(bodyParser.urlencoded({
            extended: true,
        }))
            .use("/static", serveStatic(Path.resolve(Path.join(process.cwd(), "/src/app/static"))))
            .use("/favicon.ico", serveStatic(Path.resolve(Path.join(process.cwd(), "/src/app/favicon.ico"))));
        if (this.beforeRoutesInitFn) {
            return this.beforeRoutesInitFn(this);
        }
    }
};
Server = __decorate([
    common_1.ServerSettings(config),
    __metadata("design:paramtypes", [Function])
], Server);
exports.Server = Server;
