"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
class ApiUtils {
    static getAxiosInstance() {
        return axios_1.default.create({
            baseURL: process.env.API_URL,
            headers: {
                authorization: process.env.API_ADMIN_TOKEN,
            },
        });
    }
}
exports.ApiUtils = ApiUtils;
