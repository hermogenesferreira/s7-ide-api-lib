export declare class EnderecoUtils {
    static conditionalAppend(separador: string, content?: string): string;
    static formatar(endereco?: {
        cep?: string;
        logradouro?: string;
        numero?: string;
        complemento?: string;
        bairro?: string;
        cidade?: string;
        estado?: string;
    }): string | undefined;
}
