"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ejs = require("ejs");
const path = require("path");
class EJSUtils {
    static render(type, template, dados) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                ejs.renderFile(path.join(process.cwd(), `/src/app/templates/${type}/${template}.ejs`), dados, (err, html) => {
                    if (err) {
                        reject(err);
                    }
                    else {
                        resolve(html);
                    }
                });
            });
        });
    }
}
exports.EJSUtils = EJSUtils;
