/// <reference types="node" />
export declare class ReportUtils {
    generatePDF(template: string, dados: any, dest: string): Promise<Buffer>;
}
