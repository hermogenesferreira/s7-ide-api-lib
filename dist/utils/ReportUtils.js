"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const puppeteer = require("puppeteer");
const EJSUtils_1 = require("./EJSUtils");
class ReportUtils {
    generatePDF(template, dados, dest) {
        return __awaiter(this, void 0, void 0, function* () {
            const html = yield EJSUtils_1.EJSUtils.render("reports", template, dados);
            const htmlFile = dest.replace(".pdf", ".html");
            fs.writeFileSync(htmlFile, html);
            const browser = yield puppeteer.launch({ args: ["--no-sandbox", "--disable-setuid-sandbox"] });
            const page = yield browser.newPage();
            yield page.goto(`file://${htmlFile}`, { waitUntil: ["networkidle2", "domcontentloaded"] });
            return yield page.pdf({
                path: dest,
                format: "A4",
                landscape: false,
                printBackground: true,
                margin: {
                    top: "1cm",
                    bottom: "1cm",
                    left: "1cm",
                    right: "1cm",
                },
            });
        });
    }
}
exports.ReportUtils = ReportUtils;
