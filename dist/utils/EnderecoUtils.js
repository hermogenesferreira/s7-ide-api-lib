"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class EnderecoUtils {
    static conditionalAppend(separador, content) {
        return !content ? "" : `${separador}${content}`;
    }
    static formatar(endereco) {
        if (!endereco) {
            return "";
        }
        let retorno = endereco.logradouro || "";
        retorno += this.conditionalAppend(", ", endereco.numero);
        retorno += this.conditionalAppend(" - ", endereco.complemento);
        retorno += this.conditionalAppend(" - ", endereco.bairro);
        retorno += this.conditionalAppend(" - ", endereco.cidade);
        retorno += this.conditionalAppend("/", endereco.estado);
        retorno += this.conditionalAppend(" - CEP: ", endereco.cep);
        retorno += this.conditionalAppend(" - ", endereco.complemento);
        return retorno;
    }
}
exports.EnderecoUtils = EnderecoUtils;
