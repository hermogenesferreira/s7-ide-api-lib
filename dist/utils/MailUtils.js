"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const nodemailer = require("nodemailer");
const mailgun = require("nodemailer-mailgun-transport");
const EJSUtils_1 = require("./EJSUtils");
class MailUtils {
    constructor() {
        this.sender = nodemailer.createTransport(mailgun({
            auth: {
                api_key: process.env.MAILGUN_TOKEN,
                domain: process.env.MAILGUN_DOMAIN,
            },
        }));
    }
    send(template, dados, to, assunto) {
        return __awaiter(this, void 0, void 0, function* () {
            const html = yield EJSUtils_1.EJSUtils.render("emails", template, dados);
            return this.sender.sendMail({
                to,
                html,
                from: process.env.MAILGUN_FROM,
                subject: assunto,
            });
        });
    }
}
exports.MailUtils = MailUtils;
