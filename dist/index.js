"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("path");
const Server_1 = require("./Server");
var LanguageMiddleware_1 = require("./middlewares/LanguageMiddleware");
exports.LanguageMiddleware = LanguageMiddleware_1.LanguageMiddleware;
var Server_2 = require("./Server");
exports.Server = Server_2.Server;
var MailUtils_1 = require("./utils/MailUtils");
exports.MailUtils = MailUtils_1.MailUtils;
var ReportUtils_1 = require("./utils/ReportUtils");
exports.ReportUtils = ReportUtils_1.ReportUtils;
var EJSUtils_1 = require("./utils/EJSUtils");
exports.EJSUtils = EJSUtils_1.EJSUtils;
var EnderecoUtils_1 = require("./utils/EnderecoUtils");
exports.EnderecoUtils = EnderecoUtils_1.EnderecoUtils;
var ApiUtils_1 = require("./utils/ApiUtils");
exports.ApiUtils = ApiUtils_1.ApiUtils;
var MicroserviceTokenMiddleware_1 = require("./middlewares/MicroserviceTokenMiddleware");
exports.MicroserviceTokenMiddleware = MicroserviceTokenMiddleware_1.MicroserviceTokenMiddleware;
var EntityBase_1 = require("./entities/EntityBase");
exports.EntityBase = EntityBase_1.EntityBase;
var ErrorHandlerMiddleware_1 = require("./middlewares/ErrorHandlerMiddleware");
exports.ErrorHandlerMiddleware = ErrorHandlerMiddleware_1.ErrorHandlerMiddleware;
const TypeORMLoader_1 = require("./TypeORMLoader");
var DadosPessoais_1 = require("./entities/DadosPessoais");
exports.DadosPessoais = DadosPessoais_1.DadosPessoais;
var Endereco_1 = require("./entities/Endereco");
exports.Endereco = Endereco_1.Endereco;
var ValidationMiddleware_1 = require("./middlewares/ValidationMiddleware");
exports.ValidationMiddleware = ValidationMiddleware_1.ValidationMiddleware;
const MicroserviceTokenMiddleware_2 = require("./middlewares/MicroserviceTokenMiddleware");
exports.ormLoader = TypeORMLoader_1.default;
exports.i18nLoader = (localeConfig) => {
    require("i18n").configure(Object.assign({
        locales: ["pt-BR", "en"],
        defaultLocale: "pt-BR",
        directory: path_1.join(process.cwd(), "src/app/locales"),
    }, localeConfig));
};
exports.defaultServerLoader = (beforeRoutesInitFn, localeConfig) => {
    return new Promise((resolve) => {
        // tslint:disable-next-line:no-var-requires
        require("dotenv").config();
        exports.i18nLoader(localeConfig);
        new Server_1.Server(beforeRoutesInitFn).start();
        resolve();
    });
};
exports.serverIncludeMiddlewares = (middlewares) => (serverInstance) => {
    (middlewares || []).forEach((m) => {
        serverInstance.use(m);
    });
};
exports.serverLoader = (middlewares, localeConfig) => {
    return exports.defaultServerLoader(exports.serverIncludeMiddlewares(middlewares), localeConfig);
};
exports.microserviceServerLoader = (localeConfig) => {
    exports.i18nLoader(localeConfig);
    return exports.serverLoader([MicroserviceTokenMiddleware_2.MicroserviceTokenMiddleware], localeConfig);
};
