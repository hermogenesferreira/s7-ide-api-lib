export declare class EntityBase {
    id?: number;
    createdAt?: Date;
    updatedAt?: Date;
    constructor(id?: number);
}
