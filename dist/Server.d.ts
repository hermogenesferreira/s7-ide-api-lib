import { ServerLoader } from "@tsed/common";
export declare class Server extends ServerLoader {
    private beforeRoutesInitFn?;
    constructor(beforeRoutesInitFn?: ((server: Server) => void | Promise<any>) | undefined);
    $onMountingMiddlewares(): void | Promise<any>;
}
