"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@tsed/common");
const Jwt = require("jsonwebtoken");
const TokenResolverMiddleware_1 = require("./TokenResolverMiddleware");
let MicroserviceTokenMiddleware = class MicroserviceTokenMiddleware extends TokenResolverMiddleware_1.TokenResolverMiddleware {
    resolve(request) {
        return new Promise((resolve) => {
            const token = request.headers.authorization;
            if (token) {
                Jwt.verify(token, process.env.JWT_SECRET, (err, usuario) => {
                    resolve({ isAuthenticated: !Boolean(err), decoded: { usuario } });
                });
            }
            else {
                resolve({ isAuthenticated: false });
            }
        });
    }
};
MicroserviceTokenMiddleware = __decorate([
    common_1.Middleware()
], MicroserviceTokenMiddleware);
exports.MicroserviceTokenMiddleware = MicroserviceTokenMiddleware;
