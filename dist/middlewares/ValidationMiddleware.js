"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@tsed/common");
const joi_1 = require("joi");
const path_1 = require("path");
let ValidationMiddleware = class ValidationMiddleware {
    use(request, response, endpoint, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const opcoes = require(path_1.join(process.cwd(), `src/app/validations/${endpoint.targetName}.ts`));
            joi_1.validate({
                body: request.body,
                query: request.query,
                params: request.params,
            }, opcoes[endpoint.propertyKey], {
                abortEarly: false,
                allowUnknown: true,
            }, (err) => {
                if (err) {
                    response.status(400).send(err.details);
                }
                else {
                    next();
                }
            });
        });
    }
};
__decorate([
    __param(0, common_1.Req()), __param(1, common_1.Res()), __param(2, common_1.EndpointInfo()), __param(3, common_1.Next()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, common_1.EndpointMetadata, Object]),
    __metadata("design:returntype", Promise)
], ValidationMiddleware.prototype, "use", null);
ValidationMiddleware = __decorate([
    common_1.Middleware()
], ValidationMiddleware);
exports.ValidationMiddleware = ValidationMiddleware;
