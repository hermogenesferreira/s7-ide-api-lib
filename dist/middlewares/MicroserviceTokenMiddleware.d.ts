import { TokenResolverMiddleware } from "./TokenResolverMiddleware";
export declare class MicroserviceTokenMiddleware extends TokenResolverMiddleware {
    protected resolve(request: any): Promise<{
        isAuthenticated: boolean;
        decoded?: any;
    }>;
}
