import { IMiddleware } from "@tsed/common";
export declare abstract class TokenResolverMiddleware implements IMiddleware {
    use(request: any, next: any): void | any | Promise<any>;
    protected abstract resolve(request: any): Promise<{
        isAuthenticated: boolean;
        decoded?: any;
    }>;
}
