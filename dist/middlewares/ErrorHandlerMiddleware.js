"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@tsed/common");
class ErrorHandlerMiddleware {
    use(err, request, response) {
        const error = {
            message: "",
            status: "",
            stack: "",
        };
        if (typeof err === "string") {
            error.message = err;
            error.status = "404";
        }
        else {
            error.message = err.message;
            error.status = err.status || "500";
            error.stack = err.stack;
        }
        request.log.error({
            error: {
                message: error.message,
                stack: error.stack,
                status: error.status,
            },
        });
        response.status(error.status).json(error);
        return;
    }
}
__decorate([
    __param(0, common_1.Err()), __param(1, common_1.Req()), __param(2, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Object)
], ErrorHandlerMiddleware.prototype, "use", null);
exports.ErrorHandlerMiddleware = ErrorHandlerMiddleware;
