import { EndpointMetadata, IMiddleware } from "@tsed/common";
export declare class ValidationMiddleware implements IMiddleware {
    use(request: any, response: any, endpoint: EndpointMetadata, next: any): Promise<void>;
}
