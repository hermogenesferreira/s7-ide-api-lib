import { IMiddleware } from "@tsed/common";
export declare class LanguageMiddleware implements IMiddleware {
    use(request: any, next: any): void;
}
