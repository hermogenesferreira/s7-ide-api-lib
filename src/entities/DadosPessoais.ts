import { Column } from "typeorm";

export class DadosPessoais {
  @Column({ nullable: true })
  public nome?: string;
  @Column({ nullable: true })
  public telefone?: string;
  @Column({ nullable: true })
  public email?: string;
}
