import { Column } from "typeorm";

export class Endereco {
  @Column({ nullable: true, length: 20 })
  public cep?: string;

  @Column({ nullable: true })
  public logradouro?: string;

  @Column({ nullable: true })
  public numero?: string;

  @Column({ nullable: true })
  public complemento?: string;

  @Column({ nullable: true })
  public bairro?: string;

  @Column({ nullable: true })
  public cidade?: string;

  @Column({ nullable: true, length: 2 })
  public estado?: string;

  @Column("decimal", { nullable: true, precision: 11, scale: 8 })
  public latitude?: number;

  @Column("decimal", { nullable: true, precision: 11, scale: 8 })
  public longitude?: number;
}
