import parseDbUrl = require("parse-database-url");
import { ConnectionOptions } from "typeorm";

export default (config?: any) => {
  const parsed = parseDbUrl(process.env.DATABASE_URL);
  const finalConfig = Object.assign({
    name: "default",
    type: parsed.driver,
    host: parsed.host,
    port: parsed.port,
    username: parsed.user,
    password: parsed.password,
    database: parsed.database,
    synchronize: true,
    logging: true,
    entities: ["src/app/entities/**/*.ts"],
  }, config);

  return finalConfig as ConnectionOptions;
};
