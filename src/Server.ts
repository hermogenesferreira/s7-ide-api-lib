import { GlobalAcceptMimesMiddleware, ServerLoader, ServerSettings } from "@tsed/common";
import { readFileSync } from "fs";
import Path = require("path");
import { LanguageMiddleware } from "./middlewares/LanguageMiddleware";

let config = {
  acceptMimes: ["application/json"],
  rootDir: Path.resolve(Path.join(process.cwd(), "/src/app")),
  httpPort: process.env.PORT,
  componentsScan: [
    "${rootDir}/middlewares/**/*.ts",
    "${rootDir}/services/**/*.ts",
  ],
  swagger: {
    path: "/api-docs",
  },
  mount: {
    "/": "${rootDir}/controllers/**/*.ts",
  },
};

try {
  config = Object.assign(config, JSON.parse(readFileSync(Path.join(process.cwd(), "tsed.json"), "utf8")));
// tslint:disable-next-line:no-empty
} catch {}

@ServerSettings(config)
export class Server extends ServerLoader {
  constructor(private beforeRoutesInitFn?: (server: Server) => void|Promise<any>) {
    super();
  }

  public $onMountingMiddlewares(): void|Promise<any> {

    const cookieParser = require("cookie-parser");
    const bodyParser = require("body-parser");
    const compress = require("compression");
    const methodOverride = require("method-override");
    const serveStatic = require("serve-static");

    this
      .use(GlobalAcceptMimesMiddleware)
      .use(cookieParser())
      .use(compress({}))
      .use(methodOverride())
      .use(bodyParser.json())
      .use(LanguageMiddleware)
      .use(bodyParser.urlencoded({
        extended: true,
      }))
      .use("/static", serveStatic(Path.resolve(Path.join(process.cwd(), "/src/app/static"))))
      .use("/favicon.ico", serveStatic(Path.resolve(Path.join(process.cwd(), "/src/app/favicon.ico"))));

    if (this.beforeRoutesInitFn) {
      return this.beforeRoutesInitFn(this);
    }
  }
}
