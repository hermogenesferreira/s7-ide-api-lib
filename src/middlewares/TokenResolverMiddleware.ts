import { IMiddleware, Next, Req } from "@tsed/common";

export abstract class TokenResolverMiddleware implements IMiddleware {
  public use(
    @Req() request: any,
    @Next() next: any): void | any | Promise<any> {
    this.resolve(request).then(({ isAuthenticated, decoded }) => {
      Object.assign(request, { isAuthenticated: () => isAuthenticated }, decoded || {});
      next();
    });
  }
  protected abstract resolve(request: any): Promise<{ isAuthenticated: boolean, decoded?: any }>;
}
