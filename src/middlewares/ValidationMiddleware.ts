import { EndpointInfo, EndpointMetadata, IMiddleware, Middleware, Next, Req, Res } from "@tsed/common";
import { validate } from "joi";
import { join } from "path";

@Middleware()
export class ValidationMiddleware implements IMiddleware {
  public async use(@Req() request, @Res() response, @EndpointInfo() endpoint: EndpointMetadata, @Next() next) {
    const opcoes = require(join(process.cwd(), `src/app/validations/${endpoint.targetName}.ts`));
    validate({
      body: request.body,
      query: request.query,
      params: request.params,
    }, opcoes[endpoint.propertyKey], {
      abortEarly: false,
      allowUnknown: true,
    }, (err) => {
      if (err) {
        response.status(400).send(err.details);
      } else {
        next();
      }
    });
  }
}
