import { Err, Req, Res } from "@tsed/common";

export class ErrorHandlerMiddleware {

  public use(@Err() err: any, @Req() request: any, @Res() response: any): any {
    const error = {
      message: "",
      status: "",
      stack: "",
    };

    if (typeof err === "string") {
      error.message = err;
      error.status = "404";
    } else {
      error.message = err.message;
      error.status = err.status || "500";
      error.stack = err.stack;
    }

    request.log.error({
      error: {
        message: error.message,
        stack: error.stack,
        status: error.status,
      },
    });
    response.status(error.status).json(error);
    return;
  }
}
