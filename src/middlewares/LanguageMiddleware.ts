import { IMiddleware, Middleware, Next, Request } from "@tsed/common";
import { setLocale } from "i18n";

@Middleware()
export class LanguageMiddleware implements IMiddleware {
  public use(@Request() request, @Next() next: any) {
    if (request.query.lang) {
      setLocale(request.query.lang);
    }
    next();
  }
}
