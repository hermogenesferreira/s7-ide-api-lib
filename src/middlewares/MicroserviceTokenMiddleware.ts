import {
  Middleware,
} from "@tsed/common";
import * as Jwt from "jsonwebtoken";
import { TokenResolverMiddleware } from "./TokenResolverMiddleware";

@Middleware()
export class MicroserviceTokenMiddleware extends TokenResolverMiddleware {
  protected resolve(request: any): Promise<{ isAuthenticated: boolean; decoded?: any; }> {
    return new Promise<{ isAuthenticated: boolean, decoded?: any }>((resolve) => {
      const token = request.headers.authorization;

      if (token) {
        Jwt.verify(token, process.env.JWT_SECRET as string, (err, usuario) => {
          resolve({ isAuthenticated: !Boolean(err), decoded: { usuario } });
        });
      } else {
        resolve({ isAuthenticated: false });
      }
    });
  }
}
