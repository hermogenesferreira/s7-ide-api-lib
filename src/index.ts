import { join } from "path";
import { Server } from "./Server";

export { LanguageMiddleware } from "./middlewares/LanguageMiddleware";
export { Server } from "./Server";
export { MailUtils } from "./utils/MailUtils";
export { ReportUtils } from "./utils/ReportUtils";
export { EJSUtils } from "./utils/EJSUtils";
export { EnderecoUtils } from "./utils/EnderecoUtils";
export { ApiUtils } from "./utils/ApiUtils";
export { MicroserviceTokenMiddleware } from "./middlewares/MicroserviceTokenMiddleware";
export { EntityBase } from "./entities/EntityBase";
export { ErrorHandlerMiddleware } from "./middlewares/ErrorHandlerMiddleware";
import typeORMLoader from "./TypeORMLoader";
export { DadosPessoais } from "./entities/DadosPessoais";
export { Endereco } from "./entities/Endereco";
export { ValidationMiddleware } from "./middlewares/ValidationMiddleware";
import { MicroserviceTokenMiddleware } from "./middlewares/MicroserviceTokenMiddleware";

export const ormLoader = typeORMLoader;

export const i18nLoader = (localeConfig?: any) => {
  require("i18n").configure(Object.assign({
    locales: ["pt-BR", "en"],
    defaultLocale: "pt-BR",
    directory: join(process.cwd(), "src/app/locales"),
  }, localeConfig));
};

export const defaultServerLoader = (
  beforeRoutesInitFn?: (Server) => void|Promise<any>,
  localeConfig?: any) => {
  return new Promise<void>((resolve) => {
    // tslint:disable-next-line:no-var-requires
    require("dotenv").config();
    i18nLoader(localeConfig);
    new Server(beforeRoutesInitFn).start();
    resolve();
  });
};

export const serverIncludeMiddlewares = (middlewares) => (serverInstance) => {
  (middlewares || []).forEach((m) => {
    serverInstance.use(m);
  });
};

export const serverLoader = (
  middlewares?: any[],
  localeConfig?: any) => {
  return defaultServerLoader(serverIncludeMiddlewares(middlewares), localeConfig);
};

export const microserviceServerLoader = (
  localeConfig?: any) => {
  i18nLoader(localeConfig);
  return serverLoader([MicroserviceTokenMiddleware], localeConfig);
};
