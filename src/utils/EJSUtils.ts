import * as ejs from "ejs";
import * as path from "path";

export class EJSUtils {
  public static async render(type: string, template: string, dados: any): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      ejs.renderFile(path.join(process.cwd(), `/src/app/templates/${type}/${template}.ejs`), dados, (err, html) => {
        if (err) {
          reject(err);
        } else {
          resolve(html);
        }
      });
    });
  }
}
