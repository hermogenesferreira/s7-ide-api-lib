export class EnderecoUtils {
  public static conditionalAppend(separador: string, content?: string) {
    return !content ? "" : `${separador}${content}`;
  }
  public static formatar(
  endereco?: {
    cep?: string,
    logradouro?: string,
    numero?: string,
    complemento?: string,
    bairro?: string,
    cidade?: string,
    estado?: string,
  }): string | undefined {
    if (!endereco) {
      return "";
    }

    let retorno = endereco.logradouro || "";
    retorno += this.conditionalAppend(", ", endereco.numero);
    retorno += this.conditionalAppend(" - ", endereco.complemento);
    retorno += this.conditionalAppend(" - ", endereco.bairro);
    retorno += this.conditionalAppend(" - ", endereco.cidade);
    retorno += this.conditionalAppend("/", endereco.estado);
    retorno += this.conditionalAppend(" - CEP: ", endereco.cep);
    retorno += this.conditionalAppend(" - ", endereco.complemento);

    return retorno;
  }
}
