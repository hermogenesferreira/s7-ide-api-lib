import * as fs from "fs";
import * as puppeteer from "puppeteer";

import { EJSUtils } from "./EJSUtils";

export class ReportUtils {
  public async generatePDF(template: string, dados: any, dest: string) {
    const html = await EJSUtils.render("reports", template, dados);
    const htmlFile = dest.replace(".pdf", ".html");
    fs.writeFileSync(htmlFile, html);
    const browser = await puppeteer.launch({ args: ["--no-sandbox", "--disable-setuid-sandbox"] });
    const page = await browser.newPage();
    await page.goto(`file://${htmlFile}`, { waitUntil: ["networkidle2", "domcontentloaded"] });
    return await page.pdf({
      path: dest,
      format: "A4",
      landscape: false,
      printBackground: true,
      margin: {
        top: "1cm",
        bottom: "1cm",
        left: "1cm",
        right: "1cm",
      },
    });
  }
}
