import * as nodemailer from "nodemailer";
import * as mailgun from "nodemailer-mailgun-transport";
import * as Mail from "nodemailer/lib/mailer";
import { EJSUtils } from "./EJSUtils";

export class MailUtils {
  private sender: Mail;

  constructor() {
    this.sender = nodemailer.createTransport(mailgun({
      auth: {
        api_key: process.env.MAILGUN_TOKEN,
        domain: process.env.MAILGUN_DOMAIN,
      },
    }));
  }

  public async send(template: string, dados: any, to: string, assunto: string) {
    const html = await EJSUtils.render("emails", template, dados);
    return this.sender.sendMail({
      to,
      html,
      from: process.env.MAILGUN_FROM,
      subject: assunto,
    });
  }
}
