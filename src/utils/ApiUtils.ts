import axios from "axios";

export class ApiUtils {
  public static getAxiosInstance() {
    return axios.create({
      baseURL: process.env.API_URL,
      headers: {
        authorization: process.env.API_ADMIN_TOKEN,
      },
    });
  }
}
