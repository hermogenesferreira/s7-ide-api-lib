import { expect } from "chai";
import * as Jwt from "jsonwebtoken";
import { MicroserviceTokenMiddleware } from "../../../src/middlewares/MicroserviceTokenMiddleware";

describe("MicroserviceTokenMiddleware:", () => {
  let middle: MicroserviceTokenMiddleware;

  before(() => {
    middle = new MicroserviceTokenMiddleware();
  });

  it("use > No Token", (done) => {
    const req = {
      headers: {
        authorization: undefined,
      },
    } as any;
    const next = () => { expect(req.isAuthenticated()).eq(false); done(); };
    middle.use(req, next);
  });

  it("use > Invalid Token", (done) => {
    const req = {
      headers: {
        authorization: "invalid",
      },
    } as any;
    const next = () => { expect(req.isAuthenticated()).eq(false); done(); };
    middle.use(req, next);
  });

  it("use > Authenticated", (done) => {
    const authorization = Jwt.sign(JSON.stringify({ teste: 1 }), process.env.JWT_SECRET as string);
    const req = {
      headers: { authorization },
    } as any;
    const next = () => { expect(req.isAuthenticated()).eq(true); done(); };
    middle.use(req, next);
  });
});
