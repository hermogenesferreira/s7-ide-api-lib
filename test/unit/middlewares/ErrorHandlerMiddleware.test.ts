import { expect } from "chai";
import { Conflict } from "ts-httpexceptions";
import { ErrorHandlerMiddleware } from "../../../src/middlewares/ErrorHandlerMiddleware";

describe("ErrorHandlerMiddleware:", () => {
  const middle = new ErrorHandlerMiddleware();
  const requestMock = {
    log: {
      error: (err) => null,
    },
  };

  it("use > HttpException", (done) => {
    middle.use(new Conflict("Teste"), requestMock, {
      status: (code) => {
        expect(code).eq(409);
        return {
          json: (error) => {
            expect(error.message).eq("Teste");
            done();
          },
        };
      },
    });
  });

  it("use > 404", (done) => {
    middle.use("404", requestMock, {
      status: (code) => {
        expect(code).eq("404");
        return {
          json: (error) => {
            expect(error.message).eq("404");
            done();
          },
        };
      },
    });
  });

  it("use > 500", (done) => {
    middle.use({ message: "500" }, requestMock, {
      status: (code) => {
        expect(code).eq("500");
        return {
          json: (error) => {
            expect(error.message).eq("500");
            done();
          },
        };
      },
    });
  });
});
