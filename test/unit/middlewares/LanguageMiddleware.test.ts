import { expect } from "chai";
import * as i18n from "i18n";
import * as sinon from "sinon";
import { LanguageMiddleware } from "../../../src/index";

describe("LanguageMiddleware:", () => {
  let spySetLocale;

  before(() => {
    spySetLocale = sinon.stub(i18n, "setLocale");
  });

  after(() => {
    spySetLocale.restore();
  });

  it("use > Without Locale", () => {
    const middle = new LanguageMiddleware();
    middle.use({ query: {} }, () => undefined);
    expect(spySetLocale.called).eq(false);
  });

  it("use > With Locale", () => {
    const middle = new LanguageMiddleware();
    middle.use({ query: { lang: "pt-BR" } }, () => undefined);
    expect(spySetLocale.called).eq(true);
  });
});
