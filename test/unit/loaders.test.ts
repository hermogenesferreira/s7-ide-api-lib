import { expect } from "chai";
import * as sinon from "sinon";
import * as typeorm from "typeorm";
import { microserviceServerLoader, ormLoader, serverIncludeMiddlewares } from "../../src/index";
import { Server } from "./../../src/Server";

describe("Loaders:", () => {
  let stubGetConnection: sinon.SinonStub;
  let stubCreateConn: sinon.SinonStub;
  let stubServer: sinon.SinonStub;

  before(() => {
    stubGetConnection = sinon.stub(typeorm, "getConnectionOptions").resolves({});
    stubCreateConn = sinon.stub(typeorm, "createConnection").resolves({});
    stubServer = sinon.stub(Server.prototype, "start").returns(({}));
  });

  after(() => {
    stubCreateConn.restore();
    stubGetConnection.restore();
    stubServer.restore();
  });
  it("Server Loader", () => {
    microserviceServerLoader({ directory: `${__dirname}/locales` });
  });
  it("Middlewares Loader > undefined", () => {
    serverIncludeMiddlewares(undefined)({});
  });
  it("Middlewares Loader > load", (done) => {
    serverIncludeMiddlewares([1])({
      use: () => done(),
    });
  });
  it("ORM Loader", () => {
    process.env.DATABASE_URL = "mysql://localhost:3306/app";
    const config = ormLoader();
    expect(config.type).eq("mysql");
  });
});
