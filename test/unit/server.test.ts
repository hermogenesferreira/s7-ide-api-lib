import { expect } from "chai";
import { EntityBase } from "../../src/entities/EntityBase";
import { Server } from "../../src/index";

describe("Server:", () => {
  it("instance", () => {
    const server = new Server();
    expect(server.$onMountingMiddlewares()).eq(undefined);
  });
  it("entityBase", () => {
    expect(new EntityBase(1).id).eq(1);
  });
  it("instance with beforeRoutesInitFn", (done) => {
    const server = new Server(() => {
      done();
    });
    server.$onMountingMiddlewares();
  });
});
