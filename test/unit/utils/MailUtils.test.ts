import { expect } from "chai";
import * as ejs from "ejs";
import * as nodemailer from "nodemailer";
import * as sinon from "sinon";
import { MailUtils } from "../../../src/utils/MailUtils";

describe("MailUtils:", () => {
  let stubEjs: sinon.SinonStub;
  let stubNodemailer: sinon.SinonStub;

  before(() => {
    stubEjs = sinon.stub(ejs, "renderFile").callsFake((path, dados, cb) => {
      if (path.indexOf("error.ejs") > -1) {
        cb(new Error("EJS error"));
      } else {
        cb(null, JSON.stringify(dados));
      }
    });
    stubNodemailer = sinon.stub(nodemailer, "createTransport").returns({
      sendMail: (obj) => {
        if (obj.to === "error@test.com") {
          return Promise.reject(new Error("MAILGUN error"));
        }
        return Promise.resolve({});
      },
    });
  });

  after(() => {
    stubEjs.restore();
    stubNodemailer.restore();
  });

  it("send > Render File Error", (done) => {
    const service = new MailUtils();
    service.send("error", {}, "", "").catch((err) => {
      expect(err.message).eq("EJS error");
      done();
    });
  });

  it("send > Mailgun Error", (done) => {
    const service = new MailUtils();
    service.send("template", {}, "error@test.com", "").catch((err) => {
      expect(err.message).eq("MAILGUN error");
      done();
    });
  });

  it("send > Success", (done) => {
    const service = new MailUtils();
    service.send("template", {}, "success@test.com", "").then(() => {
      done();
    });
  });
});
