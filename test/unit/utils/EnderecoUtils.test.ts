import { expect } from "chai";
import { EnderecoUtils } from "../../../src/utils/EnderecoUtils";

describe("EnderecoUtils:", () => {
  it("formatar", () => {
    const enderecoFormatado = EnderecoUtils.formatar({ logradouro: "RUA A", numero: "2" });
    expect(enderecoFormatado).eq("RUA A, 2");
  });
  it("formatar > vazio", () => {
    const enderecoFormatado = EnderecoUtils.formatar(undefined);
    expect(enderecoFormatado).eq("");
  });
  it("formatar > vazio Logradouro", () => {
    const enderecoFormatado = EnderecoUtils.formatar({});
    expect(enderecoFormatado).eq("");
  });
});
