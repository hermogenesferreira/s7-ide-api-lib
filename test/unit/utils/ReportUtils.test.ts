import { expect } from "chai";
import * as fs from "fs";
import * as puppeteer from "puppeteer";
import * as sinon from "sinon";
import { EJSUtils } from "../../../src";
import { ReportUtils } from "../../../src/utils/ReportUtils";

describe("ReportUtils:", () => {
  let stubEjs: sinon.SinonStub;
  let stubFs: sinon.SinonStub;
  let stubPuppe: sinon.SinonStub;

  before(() => {
    stubEjs = sinon.stub(EJSUtils, "render").resolves("");
    stubFs = sinon.stub(fs, "writeFileSync").returns({});
    stubPuppe = sinon.stub(puppeteer, "launch").resolves({
      newPage: () => Promise.resolve({
        goto: () => Promise.resolve({}),
        pdf: () => Promise.resolve({}),
      }),
    });
  });

  after(() => {
    stubEjs.restore();
    stubFs.restore();
    stubPuppe.restore();
  });
  it("generatePDF", (done) => {
    new ReportUtils().generatePDF("", {}, "").then(() => {
      done();
    });
  });
});
