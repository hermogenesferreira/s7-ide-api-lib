import * as i18n from "i18n";

i18n.configure({
  locales: ["pt-BR", "en"],
  defaultLocale: "pt-BR",
  directory: `${__dirname}/locales`,
});

process.env.MAILGUN_TOKEN = "test";
process.env.MAILGUN_DOMAIN = "test.com";
process.env.JWT_SECRET = "TESTE";
process.env.DATABASE_URL = "mysql://teste:teste@db:3306/db";
process.env.API_URL = "google.com";
process.env.API_ADMIN_TOKEN = "";
